<?php

use Senither\Permissionable\Permissionable;

class UserStub extends Eloquent
{
    use Permissionable;

    protected $connection = 'testbench';

    public $table = 'users';

    protected $permissionableRelationships = 'groups';

    public function groups()
    {
        return $this->belongsTo(GroupStub::class);
    }
}
