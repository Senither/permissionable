<?php

use Senither\Permissionable\Permissionable;

class GroupStub extends Eloquent
{
    use Permissionable;

    protected $connection = 'testbench';
    
    public $table = 'groups';
}
