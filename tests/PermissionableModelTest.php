<?php 

class PermissionableModelTest extends TestCase
{
    protected $john;
    protected $group;

    public function setUp()
    {
        parent::setUp();

        $this->group = GroupStub::create([
            'name' => 'Member'
        ]);

        $this->john = UserStub::create([
            'groups_id' => $this->group->id,
            'name' => 'John Doe'
        ]);

        foreach ([
            1 => 'user.test',
            2 => 'user.group.test',
            3 => 'user.group.1',
            4 => 'user.group.2',
            5 => 'user.group.3',
            6 => 'user.user.1',
            7 => 'user.user.2',
            8 => 'user.user.3',
            9 => 'user.potato.fish',
        ] as $node) {
            PermissionStub::create([
                'node' => $node
            ]);
        }
    }

    /** @test **/
    public function model_can_have_permissions()
    {
        $this->assertFalse($this->john->hasPermission('user.test'));

        $this->john->permissions()->sync([1, 6, 7, 8]);

        // Reloads the relationship from the database
        $this->john->load('permissions');

        $this->assertTrue($this->john->hasPermission('user.test'));
        $this->assertTrue($this->john->hasPermissions(['user.test', 'user.user.1', 'user.user.2', 'user.user.3']));
        $this->assertFalse($this->john->hasPermissions(['user.potato.fish', 'user.user.3']));
        
        $this->assertEquals(4, $this->john->permissions->count());
    }

    /** @test **/
    public function model_can_have_permissions_through_relationship()
    {
        $this->assertFalse($this->john->hasPermission('user.group.test'));

        $this->group->permissions()->sync([1, 2, 3, 4, 5]);

        // Reloads the relationship from the database
        $this->john->load('groups');

        $this->assertTrue($this->john->hasPermission('user.group.test'));
        $this->assertTrue($this->john->hasPermission('user.group.2'));
        $this->assertTrue($this->john->hasPermissions(['user.group.1', 'user.group.2', 'user.group.3']));

        $this->assertEquals(0, $this->john->permissions->count());
        $this->assertEquals(5, $this->john->groups->permissions->count());
    }

    /** @test **/
    public function model_can_be_found_by_permissions()
    {
        $jane = UserStub::create([
            'groups_id' => $this->group->id,
            'name' => 'Jane Doe'
        ]);

        // Give Jane permission 1, 2 & 9, and John 1, 2 & 5.
        $jane->permissions()->sync([1, 2, 9]);
        $this->john->permissions()->sync([1, 2, 5]);

        $this->assertEquals($jane->name, UserStub::withAnyPermission('user.potato.fish')->first()->name);
        $this->assertEquals($this->john->name, UserStub::withAnyPermission('user.group.3')->first()->name);
        $this->assertEquals(1, UserStub::withAllPermissions(['user.test', 'user.potato.fish'])->get()->count());
        $this->assertEquals(2, UserStub::withAllPermissions(['user.test', 'user.group.test'])->get()->count());
    }

    /** @test **/
    public function model_has_access_to_models()
    {
        $group = GroupStub::create([
            'name' => 'Test Group'
        ]);

        $this->john->permissions()->create([
            'node' => 'userstub.groupstub.' . $group->id
        ]);

        $this->assertTrue($this->john->hasAccessTo($group));
    }

    /** @test **/
    public function models_can_have_global_permissions()
    {
        $this->john->permissions()->sync([1, 2, 3, 4, 5, 6, 7, 8]);

        $this->assertTrue($this->john->hasPermission('user.*'));
        $this->assertTrue($this->john->hasPermission('user.user.*'));
        $this->assertTrue($this->john->hasPermission('user.group.*'));

        $this->assertFalse($this->john->hasPermission('user.potato.*'));
        $this->assertFalse($this->john->hasPermission('user.something.*'));
        $this->assertFalse($this->john->hasPermission('user.test.something.*'));
    }
}
