<?php

namespace Senither\Permissionable;

use Traversable;
use Illuminate\Database\Eloquent\Model;
use Senither\Permissionable\Permission;

trait Permissionable
{
    /**
     * The permissionable model instance that uses the Permissionable trait,
     * this is used in relationship calls to call itself as a relationship.
     *
     * @var Illuminate\Database\Eloquent\Model|null
     */
    protected $permissionableModelInstance;

    /**
     * Creates the polymorphic permissions relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function permissions()
    {
        return $this->morphToMany(Permission::class, 'permissionables');
    }

    /**
     * A scope to retrieve any model that has ANY OF the given permissions.
     *
     * @param  Illuminate\Database\Eloquent\Builder $query
     * @param  string|array                         $permission
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAnyPermission($query, $permission)
    {
        if ($this->isTraversable($permission)) {
            $query->hasPermissions($permission);
        }

        return $query->hasPermissions([$permission]);
    }

    /**
     * A scope to retrieve any model that has ALL the given permissions.
     *
     * @param  Illuminate\Database\Eloquent\Builder $query
     * @param  string|array                         $permissions
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAllPermissions($query, $permissions)
    {
        if (! $this->isTraversable($permissions)) {
            return $query->hasPermissions([$permissions]);
        }

        foreach ($permissions as $node) {
            $query->hasPermissions([$node]);
        }

        return $query;
    }

    /**
     * A scope to retrieve any model that has the given permissions.
     *
     * @param  Illuminate\Database\Eloquent\Builder $query
     * @param  array                                $permissions
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasPermissions($query, array $permissions)
    {
        return $query->whereHas('permissions', function ($query) use ($permissions) {
            return $query->whereIn('node', $permissions);
        });
    }

    /**
     * Checks if the provided string exists within the permissions collection
     * for the model, if an asterisk is found at the end of the string, a
     * full-search for any permission that starts with the first part
     * of the permission node provided is ran.
     *
     * Example:
     *  user.article.*  ->  user.article.1     = true
     *  user.article.*  ->  user.article.stuff = true
     *  user.article.*  ->  user.article       = false
     *
     * @param  string  $node
     * @return boolean
     */
    public function hasPermission($node)
    {
        $node = $this->getWorkablePermissionNode($node);

        // Sets the model instance so it can be called as a
        // permissable relationship if it isn't already set.
        if ($this->permissionableModelInstance == null) {
            $this->permissionableModelInstance = $this;
        }

        // Loops through all of the permissionable relationships and checks if
        // the permission node exists within any of the provided relationships.
        foreach ($this->getPermissionableRelationships() as $relationship) {
            // If the permission node isn't global(ends with an asterisk) we'll just
            // check if the full node exists in the permissions collection.
            if (! $node['isGlobal']) {
                if (! $this->{$relationship}->permissions->contains('node', $node['node'])) {
                    continue;
                }

                return true;
            }

            // Loops through all of the permissions for the current relationship and filters
            // out any permissions that doesn't start with our permission node, if the
            // collection filter returns atleast one value we'll return true.
            $filter = $this->{$relationship}->permissions->filter(function ($permission) use ($node) {
                return starts_with($permission->node, $node['node']);
            });

            if ($filter->count() > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the model has the provided list of permission nodes, the method will
     * only return true if the model has all of the provided permission nodes.
     *
     * @see Senither\Permissionable\Permissionable#hasPermissions(string)
     *
     * @param  mixed  $nodes
     * @return boolean
     */
    public function hasPermissions($nodes)
    {
        if (! $this->isTraversable($nodes)) {
            return $this->hasPermission($nodes);
        }

        foreach ($nodes as $node) {
            if (! $this->hasPermission($node)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if the model has access to the provided model by generating
     * a permission node based of their respective names.
     *
     * Example:
     *  If the model 'App\User' check if it has access to a 'App\Group' model with
     *  an id of 9, the generated permission node will look like the following:
     *
     *    user.group.9
     *
     * @param  Illuminate\Database\Eloquent\Model  $model
     * @param  string                              $identifier
     * @return boolean
     */
    public function hasAccessTo(Model $model, $identifier = 'id')
    {
        $node = [
            $this->getClassNameFor($this),
            $this->getClassNameFor($model)
        ];

        if (isset($model->{$identifier})) {
            $node[] = $model->{$identifier};
        }

        return $this->hasPermission(implode('.', $node));
    }

    /**
     * Gets a lowercase stringified version of the model.
     *
     * @param  Illuminate\Database\Eloquent\Model $class
     * @return string
     */
    protected function getClassNameFor(Model $class)
    {
        return mb_strtolower(
            collect(explode('\\', get_class($class)))->last()
        );
    }

    /**
     * Checks if the given item is traversable.
     *
     * @param  mixed  $item
     * @return boolean
     */
    protected function isTraversable($item)
    {
        return is_array($item) || ($item instanceof Traversable);
    }

    /**
     * Gets a workable version of the provided permission node
     *
     * @param  mixed $node
     * @return array
     */
    private function getWorkablePermissionNode($node)
    {
        if (is_string($node)) {
            return $this->formatWorkablePermissionNode($node);
        }

        if ($node instanceof Permission) {
            return $this->formatWorkablePermissionNode($node->node);
        }

        if ($this->isTraversable($node)) {
            return $this->getWorkablePermissionNode($node[0]);
        }

        // Assume an integer
        return $this->formatWorkablePermissionNode(
            Permission::find((int) $node)->node
        );
    }

    /**
     * Formats the provided workable node.
     *
     * @param  string $node
     * @return array
     */
    private function formatWorkablePermissionNode($node)
    {
        $workableNode = [
            'isGlobal' => ends_with($node, '*'),
            'node'   => $node,
        ];

        if ($workableNode['isGlobal']) {
            $workableNode['node'] = substr($node, 0, strlen($node) - 2);
        }

        return $workableNode;
    }

    /**
     * Gets the permissionable relationships, if the 'permissionableRelationships'
     * variable is not set an empty array is used by default, the own model
     * instance is always added to the array and is always ran first.
     *
     * @param  array  $relationships
     * @return Illuminate\Support\Collection
     */
    private function getPermissionableRelationships($relationships = [])
    {
        if (isset($this->permissionableRelationships)) {
            $relationships = (is_string($this->permissionableRelationships))
                ? [$this->permissionableRelationships]
                : $this->permissionableRelationships;
        }

        $relationships[] = 'permissionableModelInstance';

        return collect($relationships)->reverse();
    }
}
