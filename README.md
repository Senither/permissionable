# Permissionable

A simple and eloquent permission solution for Laravel models.

## Install

Using Composer

```
composer require senither/permissionable
```

Add the service provider to `config/app.php`

```php
Senither\Permissionable\PermissionableServiceProvider::class,
```

Migrate the database to create the permissionables tables.

```
artisan migrate
```

Import the `Permissionable` trait and use it in any Eloquent model.

```php
use Senither\Permissionable\Permissionable;

class User
{
    use Permissionable;
}
```

## Usage

### Check permissions

You can check if a model has a permission by calling the `hasPermission` or `hasPermissions` methods, you can also check if a model has access to another model by calling the `hasAccessTo` method.

```php
$user = User::find(1);
$post = Post::find(23);

// Returns true if the user has the permission node
if ($user->hasPermission('can.do.stuff')) {
    //
}

// Returns true if the user has ALL the permission nodes
if ($user->hasPermissions(['can.do.thing.1', 'can.do.thing.2', 'can.do.thing.3'])) {
    // 
}

// This will generate the following permission node and call 
// hasPermission behind the screens: user.post.23
if ($user->hasAccessTo($post)) {
    // 
}

```

### Get models by permissions

The `Permissionable` trait comes with two scopes that allows you to get models through the permissionable relationship, `scopeWithAnyPermission` and `scopeWithAllPermissions`.

```php
// An array, string or collection of permissions can be parsed to the scope, 
// the result will be all the users that has ANY of the permissions provided.
$users = User::withAnyPermission('can.do.stuff')->get();
$users = User::withAnyPermission(['can.do.thing.1', 'can.do.thing.2'])->get();

// This method only takes in an array or a collection of permissions as a parameter,
// the result will be all the users that has ALL of the permissions provided.
$users = User::withAllPermissions(['can.do.stuff', 'can.do.thing.3'])->get();
```

### Permissions through-relationship

If the model has any kind of relationship with another model that also uses the `Permissionable` trait you can let it know by defining a variable called `$permissionableRelationships`.

```php
class User
{
    use Senither\Permissionable\Permissionable;

    protected $permissionableRelationships = 'groups';

    public function groups()
    {
        return $this->belongsTo(Group::class);
    }
}
```
Calling the `hasPermission` or `hasPermissions` methods on the model will now also check permissions in the group the user is linked to.

> **Note:** If you have multiple relationships you want linked to the model, the `$permissionableRelationships` variable can be set to an array of strings with the method names instead.

## Issues and contribution

Just submit an issue or pull request through GitHub. Thanks!

## License

The Permissionable package for the [Laravel framework](https://laravel.com/) is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
